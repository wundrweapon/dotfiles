augroup filetype
    au! BufRead,BufNewFile *.ms,*.mm,*.mom set noai nosi noci ft=groff

    fun! UpdateGroff()
    	let file = expand('%:r')
    	let ext = expand('%:e')
    	echo '!groff -etm '.ext.' -K utf8 "'.file.'.'.ext.'" -T pdf > "'.file.'.pdf"'
    endfun

    au! BufWritePost *.ms,*.mm,*.mom call UpdateGroff()
augroup end
