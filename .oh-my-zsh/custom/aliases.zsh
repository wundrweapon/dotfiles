#Variants of ls
alias ls='ls --color=auto --group-directories-first'
alias la='ls -A'
alias ll='ls -Ahort'
alias lb='lsblk -o name,size,fstype,mountpoint'

#Ask before overwriting
alias mv='mv -iv'
alias cp='cp -iv'

#xbps helpers
alias xi='sudo xbps-install'
alias xr='sudo xbps-remove'
alias xp='sudo xbps-pkgdb'
alias xQ='xbps-query'

#Git, see also minigit OMZ plugin
alias sta='git status'
alias dif='git diff'
alias log='git log --show-signature'
alias pull='git pull'
alias push='git push'
alias cmit='git commit -v'
alias acp='git add . && cmit && push'

#Just in case my /bin symlinks change. Again.
alias lua='lua5.4'
alias luac='luac5.4'

#Miscellaneous
alias ccat='highlight -O xterm256'
alias pa='echo $((($(pamixer --get-volume) - 20)/5))'
alias py='python3'
alias wttr='curl wttr.in'
alias grep='grep --color=auto'
alias free='free -h'
alias srd='sudo sv restart dhcpcd'
alias rec='ffmpeg -f alsa -ar 48000 -ac 1 -i'
alias adisc='sudo unshare -m ~/Scripts/altdiscord.sh'
alias musictime='find . -type f | grep -ivE "nier|ware" | shuf | xargs -I {} mpv --vid=no --volume=80 "{}"'
alias ovpn='sudo openvpn'
alias da='~/.local/bin/django-admin'
#alias please='sudo $(history \!\!)'
alias gcs='gcc -std=c99 -pedantic -Wall -Wextra'
alias cbr='cargo build -r --target x86_64-unknown-linux-gnu -Z build-std=std,panic_abort -Z build-std-features=panic_immediate_abort'

#Common files I edit
alias vsc='vim ~/.config/sway/config'
alias val='vim ~/.oh-my-zsh/custom/aliases.zsh'

#Auto-pipe to less
alias -g LL='| less'

#Decompress zlib (for fun!)
alias dzlib="perl -MCompress::Zlib -e 'undef $/; print uncompress(<>)'"

#Making and changing into directories
alias mkd='mkdir -pv'
function mkcd() mkd "$1" && cd "$1"

#xbps-query alias. Gets the 32-bit shit out of the way
function xq() xbps-query "$@" | grep -v -- "-32bit"

#Make directory when moving or copying file(s) to non-extant location
function mvf() { [[ ! -e "$@[-1]" ]] && mkd "$@[-1]" && mv "$@" && return 0 || echo "$@[-1] exists" && return 1 }
function cpf() { [[ ! -e "$@[-1]" ]] && mkd "$@[-1]" && cp "$@" && return 0 || echo "$@[-1] exists" && return 1 }

#Copy or move then change directory
function cpcd() cp "$@" && cd "$@[-1]"
function mvcd() mv "$@" && cd "$@[-1]"

#Grep sans-grep
function pag() ps ax | grep -iP "^(?!.*grep).*$@"
