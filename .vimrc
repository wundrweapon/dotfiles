"Big thanks to Luke Smith (https://lukesmith.xyz/) for his public dotfiles and the video explaining most of his vimrc
"Thanks also to jamessan on stackoverflow for the filetype exemption function lol https://stackoverflow.com/a/6496995

"4-width tabs, no spaces
set ts=4 sts=0 noet sw=4

"Fuck vi
set nocp

"Smarter search casing
set ic scs

"Auto-indent
set ai si ci

"Syntax highlighting
filetype plugin on
syntax on

"Relative line numbers
set nu rnu

"UTF-8 by default
set enc=utf-8

"Call me out on line 120
set tw=120
set cc=+1

"Enable autocomplete
"TODO: find out what, if anything, this does
set wim=longest,list,full

"Deal with pesky comment formatting
autocmd FileType * setlocal fo+=t fo-=c fo-=r fo-=o

"Split on bottom and right, like i3 and any sane person
set sb spr

"Show keybinds on status bar
set sc

"Save me from myself
map q: :

"Use IJKL for cursor keys
"nnoremap i k
"nnoremap j h
"nnoremap k j

"Split navigation just via Ctrl
nnoremap <C-j> <C-w>h
nnoremap <C-k> <C-w>j
nnoremap <C-i> <C-w>k
nnoremap <C-l> <C-w>l

"Set sed regex global replace to S
nnoremap S :%s//g<Left><Left>

"Chorded Keyboard https://xkcd.com/2583/
inoremap <C-S-h><C-S-Left> hallelujah

"Autodelete trailing whitespace (ghostbytes) on save, exempting Markdown
fun! StripGhostbytes()
	if &ft =~ 'markdown'
		return
	endif

	%s/\s\+$//e
endfun

"Call the post-save function
autocmd BufWritePre * call StripGhostbytes()

"Autoupdate xrdb on editing Xresources
autocmd BufWritePost ~/.Xresources !xrdb %
